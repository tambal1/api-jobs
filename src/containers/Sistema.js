import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import {Route, Link, Switch} from 'react-router-dom';
import TableContainer from '../containers/TableContainer/TableContainer';
import NewEmployer from '../components/NewEmployer/NewEmployer';
import Employer from '../components/EmployerDetals/Employer';




class Sistema extends Component {
    constructor(props) {
        super(props);

        this.toggleNavbar = this
            .toggleNavbar
            .bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }
    render() {
        return (
            <div>
                <Navbar color="faded" light>
                    <NavbarBrand className="mr-auto">
                        <Link to="/">reactstrap
                        </Link>
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggleNavbar} className="mr-2"/>
                    <Collapse isOpen={!this.state.collapsed} navbar>
                        <Nav navbar>
                            <NavItem>
                                <NavLink>
                                    <Link to="/table"
                                exact
                                activeClassName="my-active"
                                activeStyle={{
                                    color: '#fa923f',
                                    textDecoration: 'underline'
                                }}>Home</Link>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink>
                                    <Link to="/new">New Employwe</Link>
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>

    
                <Route path="/new" component={NewEmployer}/>
                <Route path="/:id" component={Employer}/>
                <Route path="/table" component={TableContainer}/>
            </div>

        );
    }
}
export default Sistema;
