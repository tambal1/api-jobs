import React from 'react';
import axios from 'axios';

class Employer extends React.Component {
    state = {
        loadEmployer: null
    }
    componentDidMount () {
        console.log(this.props);
        this.loadData();
    }

    componentDidUpdate() {
        this.loadData();
    }

    loadData () {
        if ( this.props.match.params.id ) {
            if ( !this.state.loadEmployer || (this.state.loadEmployer && this.state.loadEmployer.id !== +this.props.match.params.id) ) {
                axios.get( 'https://dt-interviews.appspot.com/' + this.props.match.params.id )
                    .then( response => {
                        // console.log(response);
                        this.setState( { loadEmployer: response.data } );
                    } );
            }
        }
    }

    render() {
        let employer = <p hidden style={{ textAlign: 'center' }}>Please select a employer!</p>;
        if ( this.props.match.params.id ) {
            employer = <p  hidden style={{ textAlign: 'center' }}>Loading...!</p>;
        }
        if ( this.state.loadEmployer ) {
            employer = (
                <div>
                    <p>{this.state.loadEmployer.name}</p>
                    <p>{this.state.loadEmployer.title}</p>
                    
                </div>
            );
        }
        return employer;

    }
}

export default Employer;