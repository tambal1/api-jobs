import React, {Component} from 'react';
import './App.css';
import Sistema from './containers/Sistema';
import { BrowserRouter } from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Sistema />
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
