import React from 'react';
import axios from 'axios';
import {Table} from 'reactstrap';
import ListEmployments from '../../components/ListEmployments/ListEmployments';
import { Route } from 'react-router-dom';
import Employer from '../../components/EmployerDetals/Employer'

class TableContainer extends React.Component {

    state = {
        employments: [],
    }

    componentDidMount() {
        axios
            .get('https://dt-interviews.appspot.com')
            .then(response => {
                this.setState({employments: response.data});
            })
    }
    postSelectedHandle = (id) => {
        this.props.history.push('/'+ id );
    }

    render() {
        let employments = this.state.employments.map(employment => {
                let vetor = employment.name.split(',')
                return (
                    <ListEmployments
                        key={employment.id}
                        id={employment.id}
                        fName={vetor[0]}
                        lName={vetor[1]}
                        job_titles={employment.job_titles}
                        clicked={() => {
                        this.postSelectedHandle( employment.id );
                    }}/>
                )
            })
        return (

            <Table>
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>
                </thead>
                {employments}
                <Route path={this.props.match.url + '/:id'} component={Employer} />
            </Table>

        )
    }

}
export default TableContainer;
