import React from 'react';

const ListEmployments = (props) => (

    <tbody>
        <tr onClick={props.clicked}>
            <td hidden>{props.id}</td>
            <td>{props.job_titles}</td>
            <td>{props.lName}</td>
            <td>{props.fName}</td>
        </tr>
    </tbody>

);
export default ListEmployments;
